FROM nginx:alpine
COPY ./static /var/www/static
COPY ./config.nginx /etc/nginx/nginx.conf