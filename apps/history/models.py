from django.db import models


class History(models.Model):
    text = models.TextField()

    class Meta:
        db_table = 'history'
        default_permissions = ()
