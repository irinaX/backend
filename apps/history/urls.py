from rest_framework import routers

from apps.history.views import HistoryViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('history', viewset=HistoryViewSet)

urlpatterns = router.urls
