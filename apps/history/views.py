from rest_framework.viewsets import ModelViewSet

from apps.history.models import History
from apps.history.serializers import HistorySerializer


class HistoryViewSet(ModelViewSet):
    serializer_class = HistorySerializer
    queryset = History.objects.all()
