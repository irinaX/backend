# Generated by Django 3.1.7 on 2022-01-20 20:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sources_income', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='work',
            name='profitability',
        ),
    ]
