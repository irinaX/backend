from rest_framework import serializers

from apps.sources_income.models import Business, RealEstate, Work
from apps.uploads.models import Upload
from apps.uploads.serializers import UploadSerializer


class BusinessSerializer(serializers.ModelSerializer):
    image = UploadSerializer(read_only=True)
    image_id = serializers.PrimaryKeyRelatedField(queryset=Upload.objects.all(), source='image', write_only=True)

    class Meta:
        model = Business
        fields = '__all__'


class WorkSerializer(serializers.ModelSerializer):
    image = UploadSerializer(read_only=True)
    image_id = serializers.PrimaryKeyRelatedField(queryset=Upload.objects.all(), source='image', write_only=True)

    class Meta:
        model = Work
        fields = '__all__'


class RealEstateSerializer(serializers.ModelSerializer):
    image = UploadSerializer(read_only=True)
    image_id = serializers.PrimaryKeyRelatedField(queryset=Upload.objects.all(), source='image', write_only=True)

    class Meta:
        model = RealEstate
        fields = '__all__'
