from rest_framework.viewsets import ModelViewSet

from apps.sources_income.models import Business, RealEstate, Work
from apps.sources_income.serializers import BusinessSerializer, RealEstateSerializer, WorkSerializer


class BusinessViewSet(ModelViewSet):
    serializer_class = BusinessSerializer
    queryset = Business.objects.all()


class WorkViewSet(ModelViewSet):
    serializer_class = WorkSerializer
    queryset = Work.objects.all()


class RealEstateViewSet(ModelViewSet):
    serializer_class = RealEstateSerializer
    queryset = RealEstate.objects.all()
