from django.db import models


class Business(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    education = models.CharField(max_length=255)
    profitability = models.CharField(max_length=255)
    price = models.DecimalField(decimal_places=2, max_digits=12)
    min_income = models.DecimalField(decimal_places=2, max_digits=12, null=True)
    max_income = models.DecimalField(decimal_places=2, max_digits=12, null=True)
    min_time_cost = models.IntegerField(null=True)
    max_time_cost = models.IntegerField(null=True)
    business_type = models.CharField(max_length=255)
    image = models.ForeignKey('uploads.Upload',
                              null=True,
                              on_delete=models.SET_NULL)

    class Meta:
        db_table = 'businesses'
        default_permissions = ()


class Work(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    education = models.CharField(max_length=255)
    income = models.DecimalField(decimal_places=2, max_digits=12)
    time_cost = models.IntegerField(null=True)
    image = models.ForeignKey('uploads.Upload',
                              null=True,
                              on_delete=models.SET_NULL)

    class Meta:
        db_table = 'works'
        default_permissions = ()


class RealEstate(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    price = models.DecimalField(decimal_places=2, max_digits=12)
    income = models.DecimalField(decimal_places=2, max_digits=12)
    time_cost = models.IntegerField(null=True)
    perspectives = models.CharField(max_length=255)
    mortgage_term = models.IntegerField()
    mortgage_rate = models.IntegerField()
    image = models.ForeignKey('uploads.Upload',
                              null=True,
                              on_delete=models.SET_NULL)

    class Meta:
        db_table = 'real_estates'
        default_permissions = ()
