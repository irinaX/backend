from rest_framework import routers

from apps.sources_income.views import BusinessViewSet, WorkViewSet, RealEstateViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('businesses', viewset=BusinessViewSet)
router.register('works', viewset=WorkViewSet)
router.register('real_estates', viewset=RealEstateViewSet)

urlpatterns = router.urls
