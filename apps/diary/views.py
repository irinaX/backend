from rest_framework.viewsets import ModelViewSet

from apps.diary.models import Summary, Income, Cost, Asset, Liability, Time, Skill, Goal
from apps.diary.serializers import SummarySerializer, IncomeSerializer, CostSerializer, AssetSerializer, \
    LiabilitySerializer, TimeSerializer, SkillSerializer, GoalSerializer


class SummaryViewSet(ModelViewSet):
    serializer_class = SummarySerializer
    queryset = Summary.objects.all()


class IncomeViewSet(ModelViewSet):
    serializer_class = IncomeSerializer
    queryset = Income.objects.all()


class CostViewSet(ModelViewSet):
    serializer_class = CostSerializer
    queryset = Cost.objects.all()


class AssetViewSet(ModelViewSet):
    serializer_class = AssetSerializer
    queryset = Asset.objects.all()


class LiabilityViewSet(ModelViewSet):
    serializer_class = LiabilitySerializer
    queryset = Liability.objects.all()


class TimeViewSet(ModelViewSet):
    serializer_class = TimeSerializer
    queryset = Time.objects.all()


class SkillViewSet(ModelViewSet):
    serializer_class = SkillSerializer
    queryset = Skill.objects.all()


class GoalViewSet(ModelViewSet):
    serializer_class = GoalSerializer
    queryset = Goal.objects.all()
