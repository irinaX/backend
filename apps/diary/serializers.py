from rest_framework import serializers

from apps.diary.models import Summary, Income, Cost, Asset, Liability, Time, Skill, Goal
from apps.education.models import Education
from apps.education.serializers import EducationSerializer
from apps.routine.models import Family, Routine
from apps.routine.serializers import FamilySerializer, RoutineSerializer
from apps.sources_income.models import Work, RealEstate
from apps.sources_income.serializers import WorkSerializer, RealEstateSerializer


class SummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Summary
        fields = '__all__'


class IncomeSerializer(serializers.ModelSerializer):
    work = WorkSerializer(many=True, read_only=True)
    work_ids = serializers.PrimaryKeyRelatedField(
        queryset=Work.objects.all(),
        write_only=True,
        source='work',
        many=True)
    # todo: check
    businesses = WorkSerializer(many=True, read_only=True)
    businesses_ids = serializers.PrimaryKeyRelatedField(
        queryset=Work.objects.all(),
        write_only=True,
        source='businesses',
        many=True)

    class Meta:
        model = Income
        fields = '__all__'


class CostSerializer(serializers.ModelSerializer):
    family = FamilySerializer(many=True, read_only=True)
    family_ids = serializers.PrimaryKeyRelatedField(
        queryset=Family.objects.all(),
        write_only=True,
        source='family',
        many=True)

    costs = RoutineSerializer(many=True, read_only=True)
    costs_ids = serializers.PrimaryKeyRelatedField(
        queryset=Routine.objects.all(),
        write_only=True,
        source='costs',
        many=True)

    class Meta:
        model = Cost
        fields = '__all__'


class AssetSerializer(serializers.ModelSerializer):
    businesses = WorkSerializer(many=True, read_only=True)
    businesses_ids = serializers.PrimaryKeyRelatedField(
        queryset=Work.objects.all(),
        write_only=True,
        source='businesses',
        many=True)
    real_estates = RealEstateSerializer(many=True, read_only=True)
    real_estates_ids = serializers.PrimaryKeyRelatedField(
        queryset=RealEstate.objects.all(),
        write_only=True,
        source='real_estates',
        many=True)

    class Meta:
        model = Asset
        fields = '__all__'


class LiabilitySerializer(serializers.ModelSerializer):
    routines = RoutineSerializer(many=True, read_only=True)
    routines_ids = serializers.PrimaryKeyRelatedField(
        queryset=Routine.objects.all(),
        write_only=True,
        source='routines',
        many=True)

    class Meta:
        model = Liability
        fields = '__all__'


class TimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Time
        fields = '__all__'


class SkillSerializer(serializers.ModelSerializer):
    education = EducationSerializer(many=True, read_only=True)
    education_ids = serializers.PrimaryKeyRelatedField(
        queryset=Education.objects.all(),
        write_only=True,
        source='education',
        many=True)

    class Meta:
        model = Skill
        fields = '__all__'


class GoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Goal
        fields = '__all__'
