from rest_framework import routers

from apps.diary.views import SummaryViewSet, IncomeViewSet, CostViewSet, AssetViewSet, LiabilityViewSet, TimeViewSet, \
    SkillViewSet, GoalViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('summary', viewset=SummaryViewSet)
router.register('income', viewset=IncomeViewSet)
router.register('costs', viewset=CostViewSet)
router.register('assets', viewset=AssetViewSet)
router.register('liabilities', viewset=LiabilityViewSet)
router.register('times', viewset=TimeViewSet)
router.register('skills', viewset=SkillViewSet)
router.register('goals', viewset=GoalViewSet)

urlpatterns = router.urls
