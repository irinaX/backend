from django.db import models

from apps.education.models import Education
from apps.routine.models import Routine, Family
from apps.sources_income.models import Work, RealEstate


class Summary(models.Model):
    name = models.CharField(max_length=255)
    cash = models.DecimalField(decimal_places=0, max_digits=12)
    cash_flow = models.DecimalField(decimal_places=0, max_digits=12)
    hour_cost = models.DecimalField(decimal_places=0, max_digits=12)
    mood = models.CharField(max_length=255, null=True)
    game_time = models.IntegerField(null=True)
    age = models.IntegerField(null=True)

    class Meta:
        db_table = 'summary'
        default_permissions = ()


class Income(models.Model):
    all_income = models.DecimalField(decimal_places=0, max_digits=12)
    works = models.ManyToManyField(Work, related_name='work_incomes', blank=True)
    businesses = models.ManyToManyField(Work, related_name='business_incomes', blank=True)

    class Meta:
        db_table = 'income'
        default_permissions = ()


class Cost(models.Model):
    all_cost = models.DecimalField(decimal_places=0, max_digits=12)
    consumer_basket = models.PositiveIntegerField(default=10)
    family = models.ManyToManyField(Family, related_name='costs', blank=True)
    costs = models.ManyToManyField(Routine, related_name='costs', blank=True)

    class Meta:
        db_table = 'costs'
        default_permissions = ()


class Asset(models.Model):
    all_assets = models.DecimalField(decimal_places=0, max_digits=12)
    businesses = models.ManyToManyField(Work, related_name='assets', blank=True)
    real_estates = models.ManyToManyField(RealEstate, related_name='assets', blank=True)

    class Meta:
        db_table = 'assets'
        default_permissions = ()


class Liability(models.Model):
    all_liabilities = models.DecimalField(decimal_places=0, max_digits=12)
    routines = models.ManyToManyField(Routine, related_name='liabilities', blank=True)

    # todo: bank credit

    class Meta:
        db_table = 'liabilities'
        default_permissions = ()


class Time(models.Model):
    free_time = models.IntegerField()
    time_assets = models.JSONField(null=True)
    time_liabilities = models.JSONField(null=True)

    # todo: bank credit

    class Meta:
        db_table = 'times'
        default_permissions = ()


class Skill(models.Model):
    education = models.ManyToManyField(Education, related_name='skills', blank=True)

    class Meta:
        db_table = 'skills'
        default_permissions = ()


class Goal(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    price = models.DecimalField(decimal_places=0, max_digits=12, null=True)
    free_time = models.IntegerField(null=True)
    achieved = models.BooleanField(default=False)

    class Meta:
        db_table = 'goals'
        default_permissions = ()
