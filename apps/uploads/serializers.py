from django.core.files.storage import default_storage
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Upload
from .services.image_processing import optimize_img

MAX_FILE_SIZE = 10485760


def represent_image(image):
    return {
        'width': image['width'],
        'height': image['height'],
        'url': default_storage.url(image['path'])
    }


class UploadSerializer(serializers.ModelSerializer):
    file = serializers.FileField(required=False,
                                 allow_null=False,
                                 write_only=True)
    embed = serializers.CharField(required=False, write_only=True)
    url = serializers.SerializerMethodField(method_name='get_url')
    sizes = serializers.JSONField(required=False, read_only=True)

    def get_url(self, data):
        return data.path

    def create(self, validated_data, *args, **kwargs):
        if validated_data.get('embed'):
            embed = validated_data.pop('embed')
            validated_data['path'] = embed
            validated_data['type'] = 'embed'
        elif validated_data.get('file'):
            file = validated_data.pop('file')
            if file.size > MAX_FILE_SIZE:
                raise ValidationError({"file": "Слишком большой размер файла"})
            # Если файл является видео
            if file.content_type.split('/')[0] == 'video':
                raise ValidationError({"file": "Недопустимый формат файла"})

            # Если пришел файл и он не видео
            validated_data['name'] = file.name
            validated_data['type'] = file.content_type

            # Если файл является картинкой
            if file.content_type.split('/')[0] == 'image':
                file.file = file.file.read()
                validated_data['sizes'] = optimize_img(file)

            # Если файл не картинка
            else:
                storage_path = default_storage.save('files/' + file.name, file)
                validated_data['path'] = storage_path

        return self.Meta.model.objects.create(**validated_data)

    def to_representation(self, instance):
        d = {
            'id': instance.id,
            'name': instance.name,
            'type': instance.type,
        }
        if instance.sizes:
            d['sizes'] = list(map(lambda x: represent_image(x), instance.sizes))
        if instance.path:
            d['url'] = default_storage.url(instance.path)
        return d

    class Meta:
        model = Upload
        fields = ['sizes', 'file', 'url', 'name', 'id', 'embed', 'type']
        read_only_fields = ['sizes', 'url', 'id', 'embed']
