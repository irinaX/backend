import pathlib
from io import BytesIO

from PIL import Image
from django.core.files.storage import default_storage

MAX_SIDE_SIZE = 1920
PREVIEW_MAX_SIDE_SIZE = 300


def _resize_img(original_img, size):
    if original_img.width > original_img.height:
        orientation = 'horizontal'
    else:
        orientation = 'vertical'

    if orientation == 'vertical':
        tmp = size / original_img.height
        height = size
        width = original_img.width * tmp
        return original_img.resize((round(width), round(height)))
    elif orientation == 'horizontal':
        tmp = size / original_img.width
        width = size
        height = original_img.height * tmp
        return original_img.resize((round(width), round(height)))


def optimize_img(file):
    original_img = Image.open(BytesIO(file.file))
    img_format = original_img.format
    if original_img.width > MAX_SIDE_SIZE or original_img.height > MAX_SIDE_SIZE:
        original_img = _resize_img(original_img, MAX_SIDE_SIZE)
    if original_img.width > PREVIEW_MAX_SIDE_SIZE or original_img.height > PREVIEW_MAX_SIDE_SIZE:
        resized_img = _resize_img(original_img, PREVIEW_MAX_SIDE_SIZE)
    else:
        resized_img = original_img
    byte_file = BytesIO()
    original_img.save(byte_file, img_format)
    path_origin = default_storage.save('images/' + file.name, byte_file)
    byte_file.truncate(0)
    byte_file.seek(0)
    resized_img.save(byte_file, img_format)
    tmp_stem = pathlib.Path(file.name).stem
    tmp_suffix = pathlib.Path(file.name).suffix
    path_preview = default_storage.save('images/' + tmp_stem + '_' + str(PREVIEW_MAX_SIDE_SIZE) + tmp_suffix, byte_file)

    byte_file.close()
    original_img.close()
    resized_img.close()

    return [
        {
            'path': path_origin,
            'width': original_img.width,
            'height': original_img.height
        },
        {
            'path': path_preview,
            'width': resized_img.width,
            'height': resized_img.height
        }
    ]
