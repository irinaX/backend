from rest_framework import routers

from . import views

router = routers.DefaultRouter(trailing_slash=False)
router.register('upload', viewset=views.UploadViewSet)

urlpatterns = router.urls
