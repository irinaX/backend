from django.db import models
from apps.users.models import User


class Upload(models.Model):
    name = models.CharField(max_length=255, null=True, default=None)
    type = models.CharField(max_length=255, null=True, default=None)
    path = models.CharField(max_length=255, null=True, default=None)
    disk = models.CharField(max_length=255, null=True, default=None)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             null=True,
                             default=None)
    sizes = models.JSONField(null=True, default=None)
    info = models.JSONField(null=True, default=None)
    used = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'uploads'
        default_permissions = ()


# name - имя загруженного файла (test.txt)
# type - mime type (image/png, image/bmp...)
# user_id - пользователь, который загрузил (nullable)
# disk - диск, на котором хранится
# sizes - массив [{width:int, height:int, path: string}]. Тут лежат картинки в разном разрешении,
# включая оригинальную картинку.
# path - используется для embed вложений (iframe). Т.е. по сути тут html код будет лежать.  Либо тут лежит оригинал НЕ картинки
# Если вложение это картинка, то path null
# info - поле на будущее, alt и размер хранить
# user - используется ли где-нибудь этот файл.
# Например при создании категории нужно ставить user:true на его изображении.
# Это чтобы можно было удалять загруженные и не востребованные файлы через какое-то время
# Когда делается GET запрос, поля path должны подменяться на url - это http адрес,
# по которому можно открыть файл, либо код iframe
