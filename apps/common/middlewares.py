import json


class ConvertEmptyStrings:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.method in ['POST', 'PUT'] and request.content_type == 'application/json':
            body = json.loads(request.body)
            self.convert_blank_to_null(body)
            request._body = json.dumps(body).encode('utf-8')
        return self.get_response(request)

    def convert_blank_to_null(self, obj):
        keys = obj.keys() if isinstance(obj, dict) else range(len(obj))
        for key in keys:
            if isinstance(obj[key], dict) or isinstance(obj[key], list):
                self.convert_blank_to_null(obj[key])
            elif obj[key] == '':
                obj[key] = None
