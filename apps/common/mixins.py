class EagerLoadingMixin:
    def get_queryset(self):
        if hasattr(self.get_serializer(), 'setup_eager_loading'):
            return self.get_serializer().setup_eager_loading(queryset=self.queryset, action=self.action)
        else:
            return self.queryset


class RetrieveWithSlugMixin:
    def retrieve(self, request, *args, **kwargs):
        self.lookup_field = 'slug'
        kwargs['slug'] = kwargs['pk']
        self.kwargs['slug'] = kwargs['pk']
        return super().retrieve(self, request, *args, **kwargs)


class GenerateSlug:
    def save(self, *args, **kwargs):
        if self.pk is None:
            source_field = getattr(self, 'slug_source', 'name')
            source = getattr(self, source_field)
            if getattr(self, 'slug_prefix_id', False):
                next_id = self._meta.default_manager.aggregate(Max("id"))
                next_id = 1 + next_id["id__max"] if next_id["id__max"] else 1
                self.slug = f'{next_id}-{slugify(source)}'
            else:
                self.slug = slugify(source)

        return super().save(*args, **kwargs)
