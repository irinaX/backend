from rest_framework.serializers import ModelSerializer


class BaseModelSerializer(ModelSerializer):
    def __init__(self, *args, **kwargs):
        exclude = kwargs.pop('exclude', None)
        fields = kwargs.pop('fields', None)
        super(BaseModelSerializer, self).__init__(*args, **kwargs)

        if exclude and fields:
            raise ValueError("exclude argument cannot be passed together with fields arguments")

        if exclude:
            for field_name in exclude:
                self.fields.pop(field_name, None)

        elif fields:
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)
