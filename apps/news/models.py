from django.db import models


class News(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    choice = models.JSONField(null=True)
    image = models.ForeignKey('uploads.Upload',
                              null=True,
                              on_delete=models.SET_NULL)
    consequences = models.JSONField(null=True)

    class Meta:
        db_table = 'news'
        default_permissions = ()
