from rest_framework import serializers

from apps.news.models import News
from apps.uploads.models import Upload
from apps.uploads.serializers import UploadSerializer


class NewsSerializer(serializers.ModelSerializer):
    image = UploadSerializer(read_only=True)
    image_id = serializers.PrimaryKeyRelatedField(queryset=Upload.objects.all(), source='image', write_only=True)

    class Meta:
        model = News
        fields = '__all__'
