from rest_framework import routers

from apps.news.views import NewsViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('news', viewset=NewsViewSet)

urlpatterns = router.urls
