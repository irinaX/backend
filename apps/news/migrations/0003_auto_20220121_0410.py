# Generated by Django 3.1.7 on 2022-01-20 23:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_news_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='consequences',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='news',
            name='choice',
            field=models.JSONField(null=True),
        ),
    ]
