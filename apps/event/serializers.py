from rest_framework import serializers

from apps.event.models import Event
from apps.uploads.models import Upload
from apps.uploads.serializers import UploadSerializer


class EventSerializer(serializers.ModelSerializer):
    image = UploadSerializer(read_only=True)
    image_id = serializers.PrimaryKeyRelatedField(queryset=Upload.objects.all(), source='image', write_only=True)

    class Meta:
        model = Event
        fields = '__all__'
