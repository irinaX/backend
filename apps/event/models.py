from django.db import models


class Event(models.Model):
    name = models.CharField(max_length=255)
    image = models.ForeignKey('uploads.Upload',
                              null=True,
                              on_delete=models.SET_NULL)
    description = models.TextField()
    consequences = models.JSONField(null=True)
    time_cost = models.IntegerField(null=True)
    time_save = models.IntegerField(null=True)

    class Meta:
        db_table = 'events'
        default_permissions = ()
