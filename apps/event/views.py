from rest_framework.viewsets import ModelViewSet

from apps.event.models import Event
from apps.event.serializers import EventSerializer


class EventViewSet(ModelViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

