from rest_framework import routers

from apps.event.views import EventViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('events', viewset=EventViewSet)

urlpatterns = router.urls
