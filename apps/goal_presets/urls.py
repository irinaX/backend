from rest_framework import routers

from apps.goal_presets.views import GoalPresetViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('goal_presets', viewset=GoalPresetViewSet)

urlpatterns = router.urls
