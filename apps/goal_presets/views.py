from rest_framework.viewsets import ModelViewSet

from apps.goal_presets.models import GoalPreset
from apps.goal_presets.serializers import GoalPresetSerializer


class GoalPresetViewSet(ModelViewSet):
    serializer_class = GoalPresetSerializer
    queryset = GoalPreset.objects.all()
