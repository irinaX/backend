from django.db import models


class GoalPreset(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    price = models.DecimalField(decimal_places=0, max_digits=12)
    image = models.ForeignKey('uploads.Upload',
                              null=True,
                              on_delete=models.SET_NULL)

    class Meta:
        db_table = 'goal_presets'
        default_permissions = ()
