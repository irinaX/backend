from rest_framework import serializers

from apps.goal_presets.models import GoalPreset
from apps.uploads.models import Upload
from apps.uploads.serializers import UploadSerializer


class GoalPresetSerializer(serializers.ModelSerializer):
    image = UploadSerializer(read_only=True)
    image_id = serializers.PrimaryKeyRelatedField(queryset=Upload.objects.all(), source='image', write_only=True)

    class Meta:
        model = GoalPreset
        fields = '__all__'
