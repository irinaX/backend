from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.core import validators
from django.core.exceptions import ValidationError
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, phone, password=None, **kwargs):
        user = self.model(phone=phone, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, phone, password=None, **kwargs):
        user = self.model(phone=phone, is_superuser=True, is_employee=True, **kwargs)
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    password = models.CharField(max_length=255)
    email = models.EmailField(
        validators=[
            validators.validate_email],
        unique=True,
        null=True,
        error_messages={
            'unique': "Пользователь с такой электронной почтой уже зарегистрирован."})
    first_name = models.CharField(max_length=255, blank=False, null=False)
    last_name = models.CharField(max_length=255, blank=False, null=False)
    phone = models.CharField(
        max_length=255, null=False, blank=False, unique=True, error_messages={
            'unique': "Пользователь с таким телефоном уже зарегистрирован."})
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField('active', default=True)
    is_employee = models.BooleanField(default=False)

    USERNAME_FIELD = 'phone'

    REQUIRED_FIELDS = ['first_name']

    objects = UserManager()

    def delete(self, using=None, keep_parents=False):
        if self.is_superuser:
            raise ValidationError('Нельзя удалить суперпользователя')
        return super(User, self).delete(using, keep_parents)

    class Meta:
        db_table = 'users'
        permissions = (
            ('update_users', 'Редактирование пользователей'),
            ('update_employees', 'Редактирование сотрудников'),
        )
        default_permissions = ()
