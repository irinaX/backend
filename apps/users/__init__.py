from django.apps import AppConfig


class UsersAppConfig(AppConfig):
    name = 'apps.users'
    label = 'users'

    # def ready(self):
    #     import apps.users.signals


default_app_config = 'apps.users.UsersAppConfig'
