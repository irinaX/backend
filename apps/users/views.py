import datetime
import random
import string

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Permission
from django.core import signing
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.http import Http404
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import api_view
from rest_framework.filters import SearchFilter
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework_jwt.settings import api_settings

from .models import User
from .permissions import HasPermission
from .serializers import UserSerializer, PermissionSerializer, ProfileSerializer, SignUpSerializer
from ..common.mixins import EagerLoadingMixin


# class EmployeeViewSet(EagerLoadingMixin, ModelViewSet):
#     serializer_class = UserSerializer
#     queryset = User.objects.filter(is_employee=True)


class UserViewSet(EagerLoadingMixin, ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    filter_backends = [SearchFilter, DjangoFilterBackend]
    search_fields = ['last_name', 'first_name', 'middle_name', 'phone', 'email']
    filterset_fields = ['is_employee']

    def get_permissions(self):
        if self.action in ['create']:
            permissions = [IsAuthenticated(), HasPermission('users.update')]
            if self.request.data.get('is_employee'):
                permissions.append(HasPermission('users.assign_employee'))
            return permissions

        if self.action in ['update', 'destroy']:
            permissions = [IsAuthenticated(), HasPermission('users.update')]
            try:
                is_object_employee = User.objects.values('is_employee').get(pk=self.kwargs[self.lookup_field])[
                    'is_employee']
            except ObjectDoesNotExist:
                raise Http404
            if self.request.data.get('is_employee') or is_object_employee:
                permissions.append(HasPermission('users.assign_employee'))
                return permissions

        elif self.action in ['list', 'receive']:
            return [IsAuthenticated(), HasPermission('users.view')]

        return [IsAuthenticated()]


class PermissionViewSet(ListModelMixin, GenericViewSet):
    queryset = Permission.objects.exclude(content_type__app_label__in=['auth', 'contenttypes', 'sessions', 'silk'])
    serializer_class = PermissionSerializer


def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': ProfileSerializer().to_representation(user)
    }


@api_view(['POST'])
def register(request):
    serializer = SignUpSerializer(data=request.data)
    serializer.is_valid(True)
    instance = serializer.create(validated_data=serializer.validated_data)
    return get_user_token_response(instance)


def get_user_token_response(user):
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return Response({
        'token': token,
        'user': ProfileSerializer().to_representation(user)
    })


@api_view(['POST'])
def reset_password(request):
    user = User.objects.get(email=request.data['email'])

    letters = string.ascii_letters
    new_password = ''.join(random.choice(letters) for i in range(8))
    user.password = make_password(new_password)
    user.save()

    html_message = render_to_string('password-restoration.html', {
        'password': new_password
    })
    plain_message = strip_tags(html_message)
    send_mail(subject='Пароль сброшен',
              message=plain_message,
              html_message=html_message,
              from_email='robot@1creator.ru',
              recipient_list=[user.email])
    return Response(1)


@api_view(['POST'])
def confirm_email(request):
    data = signing.loads(request.data['data'])
    user = User.objects.get(pk=data['user_id'])
    if user.email_verified_at:
        raise Http404
    user.email_verified_at = datetime.datetime.now()
    user.save()
    return get_user_token_response(user)


@api_view(['PUT', 'GET', 'DELETE'])
def profile(request):
    if request.method == 'GET':
        serializer = ProfileSerializer(instance=request.user)
        return Response(serializer.to_representation(instance=request.user))
    elif request.method == 'PUT':
        serializer = ProfileSerializer(instance=request.user, data=request.data)
        serializer.is_valid(True)
        instance = serializer.update(instance=request.user, validated_data=serializer.validated_data)
        return Response(serializer.to_representation(instance=instance))
    elif request.method == 'DELETE':
        request.user.delete()
        return Response(1)


def notify_admins(subject, html_message):
    recipients = User.objects.filter(is_employee=True, email_verified_at__isnull=False).values_list('email', flat=True)
    plain_message = strip_tags(html_message)
    return send_mail(subject=subject,
                     message=plain_message,
                     html_message=html_message,
                     from_email='robot@1creator.ru',
                     recipient_list=recipients)


def notify_user(user, subject, html_message):
    if not user.email or not user.email_verified_at:
        return False
    plain_message = strip_tags(html_message)
    return send_mail(subject=subject,
                     message=plain_message,
                     html_message=html_message,
                     from_email='robot@1creator.ru',
                     recipient_list=[user.email])
