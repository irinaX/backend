from django.conf.urls import url
from django.urls import include
from django.urls import path
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from . import views

router = routers.DefaultRouter(trailing_slash=False)
router.register('users', viewset=views.UserViewSet)
router.register('permissions', viewset=views.PermissionViewSet)

urlpatterns = [
    url('login', obtain_jwt_token),
    url('register', views.register),
    url('profile', views.profile),
    url('reset-password', views.reset_password),
    url('email-confirmation', views.confirm_email),
    path('', include(router.urls)),
]
