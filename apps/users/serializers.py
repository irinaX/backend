from venv import create

from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth.models import Permission
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from ..common.serializers import BaseModelSerializer
from .models import User


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = '__all__'


class ProfileSerializer(BaseModelSerializer):
    password = serializers.CharField(required=False, min_length=6, allow_blank=False, write_only=True)
    password_confirmation = serializers.CharField(required=False, min_length=6, allow_blank=False, write_only=True)
    old_password = serializers.CharField(required=False, allow_blank=False, write_only=True)
    first_name = serializers.CharField(required=False, allow_blank=False)
    last_name = serializers.CharField(required=False, allow_blank=False,
                                      error_messages={'required': 'Необходимо заполнить поле Фамилия.',
                                                      'null': 'Необходимо заполнить поле Фамилия.'})
    email = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    phone = serializers.CharField(required=False, allow_blank=False)
    permissions = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_permissions(user):
        if user.is_employee:
            return user.get_user_permissions()
        return None

    def validate(self, data):
        if 'password' in data:
            if data.get('password') != data.get('password_confirmation'):
                raise ValidationError({'password_confirmation': 'Пароль не совпадает с подтверждением.'})
            if not check_password(data.get('old_password'), self.instance.password):
                raise ValidationError({'old_password': 'Указан неверный старый пароль.'})

            data['password'] = make_password(data.get('password'))
        return data

    class Meta:
        model = User
        exclude = ['user_permissions']


class UserSerializer(BaseModelSerializer):
    password = serializers.CharField(write_only=True, required=False, allow_null=True)
    permissions = PermissionSerializer(many=True, source='user_permissions', read_only=True)
    permission_ids = serializers.PrimaryKeyRelatedField(queryset=Permission.objects.all(), many=True, write_only=True,
                                                        source='permissions', required=False)

    def validate(self, data):
        if not self.instance and 'password' not in data:
            raise ValidationError({'password': 'Password required!'})

        if 'password' in data:
            if data.get('password') != self.initial_data.get('password_confirmation'):
                raise ValidationError({'password_confirmation': 'Пароль не совпадает с подтверждением.'})
            data['password'] = make_password(data.get('password'))

        return data

    def create(self, validated_data):
        permissions = validated_data.pop('permissions', [])
        instance = super().create(validated_data)
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        if instance.is_employee:
            instance.user_permissions.set(permissions, clear=True)

        return instance

    def update(self, instance, validated_data):
        permissions = validated_data.pop('permissions', [])

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        if instance.is_employee:
            instance.user_permissions.set(permissions, clear=True)

        return instance

    def to_representation(self, instance):
        user = super().to_representation(instance)
        if not user['is_employee']:
            user.pop('position', None)
            user.pop('permissions', None)

        return user

    class Meta:
        model = User
        exclude = ['user_permissions', 'groups']
        extra_kwargs = {
            "first_name": {
                "error_messages": {
                    "required": "Необходимо заполнить поле Имя.",
                    "null": "Необходимо заполнить поле Имя.",
                }
            },
            "last_name": {
                "error_messages": {
                    "required": "Необходимо заполнить поле Фамилия.",
                    "null": "Необходимо заполнить поле Фамилия.",
                }
            },
            "phone": {
                "error_messages": {
                    "required": "Необходимо заполнить поле Телефон.",
                    "null": "Необходимо заполнить поле Телефон.",
                    "unique": "Поле Телефон должно быть уникальным.",
                }
            },
        }


class SignUpSerializer(BaseModelSerializer):
    password = serializers.CharField(write_only=True, required=True)

    def validate(self, data):
        if data.get('password') != self.initial_data.get('password_confirmation'):
            raise ValidationError({'password_confirmation': 'Пароль не совпадает с подтверждением.'})
        data['password'] = make_password(data.get('password'))
        return data

    def to_representation(self, instance):
        user = super().to_representation(instance)
        if not user['is_employee']:
            user.pop('position', None)
            user.pop('permissions', None)

        return user

    class Meta:
        model = User
        exclude = ['user_permissions', 'groups']
        extra_kwargs = {
            "name": {
                "error_messages": {
                    "required": "Необходимо заполнить поле Имя.",
                    "null": "Необходимо заполнить поле Имя.",
                }
            },
            "phone": {
                "error_messages": {
                    "required": "Необходимо заполнить поле Телефон.",
                    "null": "Необходимо заполнить поле Телефон.",
                    "unique": "Поле Телефон должно быть уникальным.",
                }
            },
            "password": {
                "error_messages": {
                    "required": "Необходимо заполнить поле Пароль.",
                    "null": "Необходимо заполнить поле Пароль.",
                    "blank": "Необходимо заполнить поле Пароль.",
                }
            },
        }
