from django.conf import settings
from django.core import signing
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from apps.users.models import User


@receiver(post_save, sender=User)
def send_email_confirmation_link(sender, instance, created, **kwargs):
    if created:
        confirmation_data = signing.dumps({"user_id": instance.id})
        confirmation_link = f'{settings.FRONTEND_URL}/email-confirmation?data={confirmation_data}'

        html_message = render_to_string('email-confirmation.html', {
            'confirmation_link': confirmation_link
        })
        plain_message = strip_tags(html_message)
        send_mail(subject='Аккаунт зарегистрирован',
                  message=plain_message,
                  html_message=html_message,
                  from_email='robot@1creator.ru',
                  recipient_list=[instance.email])
