from rest_framework.permissions import BasePermission


class HasPermission(BasePermission):
    def __init__(self, permission):
        self.permission = permission

    def has_permission(self, request, view):
        return request.user.has_perm(self.permission)

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)


class IsEmployee(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_employee

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)
