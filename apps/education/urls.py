from rest_framework import routers

from apps.education.views import EducationViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('education', viewset=EducationViewSet)

urlpatterns = router.urls
