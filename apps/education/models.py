from django.db import models


class Education(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    benefit = models.JSONField(null=True, default=dict)
    price = models.DecimalField(decimal_places=2, max_digits=12)
    time_cost = models.IntegerField(default=0)
    training_period = models.IntegerField(default=0)

    class Meta:
        db_table = 'education'
        default_permissions = ()
