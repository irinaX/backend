from rest_framework.viewsets import ModelViewSet

from apps.education.models import Education
from apps.education.serializers import EducationSerializer


class EducationViewSet(ModelViewSet):
    serializer_class = EducationSerializer
    queryset = Education.objects.all()
