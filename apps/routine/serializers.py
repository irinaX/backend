from rest_framework import serializers

from apps.routine.models import Routine, Family
from apps.uploads.models import Upload
from apps.uploads.serializers import UploadSerializer


class RoutineSerializer(serializers.ModelSerializer):
    image = UploadSerializer(read_only=True)
    image_id = serializers.PrimaryKeyRelatedField(queryset=Upload.objects.all(), source='image', write_only=True)

    class Meta:
        model = Routine
        fields = '__all__'


class FamilySerializer(serializers.ModelSerializer):
    class Meta:
        model = Family
        fields = '__all__'
