from rest_framework.viewsets import ModelViewSet

from apps.routine.models import Routine, Family
from apps.routine.serializers import RoutineSerializer, FamilySerializer


class RoutineViewSet(ModelViewSet):
    serializer_class = RoutineSerializer
    queryset = Routine.objects.all()


class FamilyViewSet(ModelViewSet):
    serializer_class = FamilySerializer
    queryset = Family.objects.all()

