from django.db import models


class Routine(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    cost_percent = models.IntegerField(null=True)
    cost_value = models.DecimalField(decimal_places=2, max_digits=12, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=12, null=True)
    time_cost = models.IntegerField(null=True)
    time_save = models.IntegerField(null=True)
    routine_type = models.CharField(max_length=255)
    image = models.ForeignKey('uploads.Upload',
                              null=True,
                              on_delete=models.SET_NULL)

    class Meta:
        db_table = 'routines'
        default_permissions = ()


class Family(models.Model):
    name = models.CharField(max_length=255)
    age = models.IntegerField()
    cost_percent = models.IntegerField(null=True)
    time_cost = models.IntegerField(null=True)

    class Meta:
        db_table = 'family'
        default_permissions = ()
