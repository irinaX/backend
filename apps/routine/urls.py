from rest_framework import routers

from apps.routine.views import RoutineViewSet, FamilyViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('routines', viewset=RoutineViewSet)
router.register('family', viewset=FamilyViewSet)

urlpatterns = router.urls
