# Generated by Django 3.1.7 on 2022-01-20 20:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('presets', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='preset',
            name='summary',
        ),
        migrations.AddField(
            model_name='preset',
            name='age',
            field=models.IntegerField(default=18),
        ),
        migrations.AddField(
            model_name='preset',
            name='cash',
            field=models.DecimalField(decimal_places=0, default=0, max_digits=12),
        ),
        migrations.AddField(
            model_name='preset',
            name='cash_flow',
            field=models.DecimalField(decimal_places=0, default=0, max_digits=12),
        ),
        migrations.AddField(
            model_name='preset',
            name='free_time',
            field=models.IntegerField(default=0),
        ),
    ]
