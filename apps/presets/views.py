from rest_framework.viewsets import ModelViewSet

from apps.presets.models import Preset
from apps.presets.serializers import PresetSerializer


class PresetViewSet(ModelViewSet):
    serializer_class = PresetSerializer
    queryset = Preset.objects.all()
