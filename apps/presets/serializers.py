from rest_framework import serializers

from apps.education.models import Education
from apps.education.serializers import EducationSerializer
from apps.presets.models import Preset
from apps.routine.models import Routine, Family
from apps.routine.serializers import RoutineSerializer, FamilySerializer
from apps.sources_income.models import Work
from apps.sources_income.serializers import WorkSerializer
from apps.uploads.models import Upload
from apps.uploads.serializers import UploadSerializer


class PresetSerializer(serializers.ModelSerializer):
    image = UploadSerializer(read_only=True)
    image_id = serializers.PrimaryKeyRelatedField(queryset=Upload.objects.all(), source='image', write_only=True)
    work = WorkSerializer(many=True, read_only=True)
    work_ids = serializers.PrimaryKeyRelatedField(
        queryset=Work.objects.all(),
        write_only=True,
        source='work',
        many=True)
    routines = RoutineSerializer(many=True, read_only=True)
    routines_ids = serializers.PrimaryKeyRelatedField(
        queryset=Routine.objects.all(),
        write_only=True,
        source='routines',
        many=True)
    education = EducationSerializer(many=True, read_only=True)
    education_ids = serializers.PrimaryKeyRelatedField(
        queryset=Education.objects.all(),
        write_only=True,
        source='education',
        many=True)
    family = FamilySerializer(many=True, read_only=True)
    family_ids = serializers.PrimaryKeyRelatedField(
        queryset=Family.objects.all(),
        write_only=True,
        source='family',
        many=True)

    class Meta:
        model = Preset
        fields = '__all__'
