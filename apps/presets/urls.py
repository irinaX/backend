from rest_framework import routers

from apps.presets.views import PresetViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register('presets', viewset=PresetViewSet)

urlpatterns = router.urls
