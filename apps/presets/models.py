from django.db import models

from apps.education.models import Education
from apps.routine.models import Routine, Family
from apps.sources_income.models import Work


class Preset(models.Model):
    image = models.ForeignKey('uploads.Upload',
                              null=True,
                              on_delete=models.SET_NULL)
    name = models.CharField(max_length=255)
    description = models.TextField()
    cash = models.DecimalField(decimal_places=0, max_digits=12)
    cash_flow = models.DecimalField(decimal_places=0, max_digits=12)
    free_time = models.IntegerField()
    age = models.IntegerField()
    work = models.ManyToManyField(Work, related_name='presets', blank=True)
    routines = models.ManyToManyField(Routine, related_name='presets', blank=True)
    education = models.ManyToManyField(Education, related_name='presets', blank=True)
    consumer_basket = models.PositiveIntegerField(default=10)
    family = models.ManyToManyField(Family, related_name='presets', blank=True)

    class Meta:
        db_table = 'presets'
        default_permissions = ()
